package me.arenm.tempconv;

public class Converter {
    public static double convertToFahrenheit(String celsius) {
        return (Double.parseDouble(celsius) * (9.0/5.0)) + 32;
    }
}
